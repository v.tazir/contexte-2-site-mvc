<?php

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    $racine = "..";
}
include_once "$racine/modele/bd.typecuisine.inc.php";
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.aimer.inc.php";

$mailU = getMailULoggedOn();

// recuperation des donnees GET, POST, et SESSION
if (isset($_POST["pseudo"])){
    modifPseudo($mailU, $_POST["pseudo"]);
}

else if (isset($_POST["newMdp"]) && isset($_POST["confirmMdp"])){
    if($_POST["newMdp"] == $_POST["confirmMdp"]){
        modifMdp($mailU, $_POST["newMdp"]);
    }
}

else if (isset($_POST["supprResto"])){    
    foreach ($_POST["supprResto"] as $sr){
        delAimer($mailU, $sr);
    }
}

else if (isset($_POST["supprTypeC"])){
    foreach ($_POST["supprTypeC"] as $stc){
        delTypeCuisine($mailU, $stc);
    }
}

else if (isset($_POST["ajoutTypeC"])){
    foreach ($_POST["ajoutTypeC"] as $atc){
        addTypeCuisine($mailU, $atc);
    }
}
// appel des fonctions permettant de recuperer les donnees utiles a l'affichage 
$cuisinesAimes = getTypesCuisinePreferesByMailU($mailU);
$cuisinesNonAimes = getTypesCuisineNonPreferesByMailU($mailU);
$restosAimes = getAimerByMailU($mailU);

$titre = "Mon profil";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueModifProfil.php";
include "$racine/vue/pied.html.php";
?>
