<?php

include_once "bd.inc.php";

function getAimerByMailU($mailU) {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from site_mvc.aimer a inner join site_mvc.resto r on r.id=a.id_r where mail=:mailU");
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        $req->execute();
        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getAimerByIdR($idR) {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from site_mvc.aimer where id_r=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getAimerById($mailU, $idR){
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from site_mvc.aimer where mail=:mailU and  id_r=:idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        
        $req->execute();
        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function addAimer($mailU, $idR) {
    $resultat = -1;
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into site_mvc.aimer (mail, id_r) values(:mailU,:idR)");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function delAimer($mailU, $idR) {
    $resultat = -1;
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("delete from site_mvc.aimer where id_r=:idR and mail=:mailU");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}


if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "\n addAimer(\"geraldine.taysse@gmail.com\",1) : \n";
    print_r(addAimer("geraldine.taysse@gmail.com", 1));

    echo "\n addAimer(\"geraldine.taysse@gmail.com\",2) : \n";
    print_r(addAimer("geraldine.taysse@gmail.com", 2));

    echo "\n getAimerByMailU(\"geraldine.taysse@gmail.com\") : \n";
    print_r(getAimerByMailU("geraldine.taysse@gmail.com"));

    echo "\n getAimerByIdR(1) : \n";
    print_r(getAimerByIdR(1));
    
    echo "\n delAimer(\"geraldine.taysse@gmail.com\",2) : \n";
    print_r(delAimer("geraldine.taysse@gmail.com", 2));

    
}
?>