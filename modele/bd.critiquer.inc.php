<?php

include_once "bd.inc.php";

function getCritiquerByIdR($idR) {
    $resultat = array();
    
    // completer le code manquant (question 3.3)
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from site_mvc.critiquer where id_r = :idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->execute();

        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }  
    
    return $resultat;
}

function getNoteMoyenneByIdR($idR) {
  
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select SUM(note) / COUNT(*)::float as moyenne from site_mvc.critiquer where id_r = :idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    if ($resultat["moyenne"] == NULL){
        $resultat["moyenne"] = 0.0;
    }
    
    return $resultat["moyenne"];
}

function delCritiquer($mailU, $idR) {

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("DELETE FROM site_mvc.critiquer WHERE mail = :mailU AND id_r = :idR");
        $req->bindValue(':idR', $idR, PDO::PARAM_INT);
        $req->bindValue(':mailU', $mailU, PDO::PARAM_STR);
        $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $req;
}

?>
