<h1>Recherche d'un restaurant</h1>
<form action="./?action=recherche&critere=<?= $critere ?>" method="POST">

    <?php
    switch ($critere) {
        case "nom":
            ?>
            Recherche par nom : <br />
            <input type="text" name="nomR" placeholder="nom" value="<?= $nomR ?>" /><br />
            <?php
            break;
        case "adresse":
            ?>
            Recherche par adresse : <br />
            <input type="text" name="villeR" placeholder="ville" value="<?= $villeR ?>"/><br />
            <input type="text" name="cpR" placeholder="code postal" value="<?= $cpR ?>"/><br />
            <input type="text" name="voieAdrR" placeholder="rue" value="<?= $voieAdrR ?>"/><br />
            <?php
            break;
        
		case "type" :
		?>
			Recherche par type de cuisine : <br />
		<?php
			foreach($lesTypesCuisines as $tc){
			?>
				<input type="checkbox" name="types[]" id="<?= $tc['id_tc'] ?>" value="<?= $tc['id_tc'] ?>"
				<?php
					if(in_array($tc['id_tc'], $idTypes)){
						echo 'checked';
					}
				?>		
				/>
				<label for="<?= $tc['id_tc'] ?>"><?= $tc['libelle'] ?></label><br/>
			<?php
			}
    }
    ?>
    <br />
    <input type="submit" name="Rechercher" value="Rechercher" />

</form>
