<h1>Modifier mon profil</h1>
Mon adresse électronique : <?= $mailU ?>
<br>
<form action="" method="post">
    Mettre à jour mon pseudo : 
    <br>
    <input type="text" id="pseudo" name="pseudo" placeholder="Nouveau pseudo">
    <br>
    <input type="submit" value="Enregistrer">
</form>
<hr>
<form action="" method="post">
    Mettre à jour mon mot de passe :
    <br>
    <input type="text" id="newMdp" name="newMdp" placeholder="Nouveau mot de passe">
    <br>
    <input type="text" id="confirmMdp" name="confirmMdp" placeholder="Confirmer la saisie">
    <br>
    <input type="submit" value="Enregistrer">
</form>
<hr>
<form action="" method="post">
    Gerer les restaurants que j'aime :
    <br>
    <?php foreach($restosAimes as $ras){    
        echo '<input type="checkbox" value="'.$ras["id"].'" name="supprResto[]">'
        . '<label for="'.$ras["id"].'">'.$ras["nom"].'</label><br>';    
    }?>
    <input type="submit" value="Supprimer">
</form>
<hr>
<form action="" method="post">
    Les types de cuisine que j'aime :
    <br>
    <?php foreach ($cuisinesAimes as $cas) {
        echo '<input type="checkbox" value="'.$cas["id_tc"].'" name="supprTypeC[]">'
        . ' <label for="'.$cas["id_tc"].'"><li class="tag"><span class="tag">#</span><i>'.$cas["libelle"].'</i></li></label><br />';
    } ?>
    <input type="submit" value="Supprimer">    
</form>
<hr>
<form action="" method="post">
    Choisir d'autres types de cuisine :
    <br>
    <?php foreach ($cuisinesNonAimes as $cnas) {
        echo '<input type="checkbox" value="'.$cnas["id_tc"].'" name="ajoutTypeC[]">'
        . ' <label for="'.$cnas["id_tc"].'"><li class="tag"><span class="tag">#</span><i>'.$cnas["libelle"].'</i></li></label><br />';
    } ?>
    <input type="submit" value="Ajouter"> 
</form>