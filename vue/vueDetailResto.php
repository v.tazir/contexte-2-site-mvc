<h1> <?= $unResto["nom"];   
    
    if (isLoggedOn()){
        $etoile = getAimerByMailU($mailU);
        $etoile_ou_pas = false; 
        foreach ($etoile as $e) {
            if ($e["id_r"] == $unResto["id"]){
                $etoile_ou_pas = true;                
            }
        }
        if ($etoile_ou_pas){
            echo '<a href="./?action=aimer&idR='.$unResto["id"].'"> <img src="images/aime.png" alt="photo" /> </a>';
        }
        else {
            echo '<a href="./?action=aimer&idR='.$unResto["id"].'"> <img src="images/aimepas.png" alt="photo" /> </a>';
        }
    }
?> </h1>

<span id="note">
</span>
<section>
    Cuisine <br />
    <ul id="tagFood">		
        <?php
         foreach ($lesTypesCuisine as $tc) {
             echo '<br>- '.$tc["libelle"];
         }
        ?>
    </ul>

</section>
<p id="principal">
    <?php echo '<img src="photos/'.$lesPhotos[0]["chemin"].'" alt="photo" />';?>         
    <br />        
	<?= $unResto["desc_resto"];?>       

</p>
<h2 id="adresse">
    Adresse
</h2>
<p>
	<?= $unResto["num_adr"].' '.$unResto["voie_adr"].'<br>'.$unResto["cp"].' '.$unResto["ville"];?>

</p>

<h2 id="photos">
    Photos
</h2>
<ul id="galerie">    
    <?php
    if (count($lesPhotos) > 0){
        foreach ($lesPhotos as $p){
            echo '<img src="photos/'.$p["chemin"].'" alt="photo" /> <br>';
        }
    }
    ?>
</ul>

<h2 id="horaires">
    Horaires
</h2> 
	<?= $unResto["horaires"];?>


<h2 id="crit">Critiques</h2>

<ul id="critiques">
    <?= 'Moyenne des notes du restaurant : '.$noteMoy;  
 foreach ($critiques as $ctq) {
    ?>
    <li>
        <span>
            <?=$ctq['mail'];
            if(isLoggedOn() && $_SESSION["mailU"] == $ctq['mail']){
                echo ' <a href="./?action=supprCrit&idR='.$ctq["id_r"].'">Supprimer</a>';
            }
        ?> <!--+ lien Supprimer seulement si l’adresse mail correspond à l’utilisateur connecté -->
        </span>
        <div>
            <span> <?=$ctq["note"].'/5'; ?></span>           
            <span> <?=' - '.$ctq["commentaire"]; ?></span>
        </div>
    </li>
    <?php } ?>
</ul>

